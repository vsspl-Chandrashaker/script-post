package transaction.script.productionData.Curing;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import transaction.script.platform.utils.PostgresDBUtil;

public class CureProductionDataFromMaster {
	public static ArrayList<String> filters = new ArrayList<String>(Arrays.asList("solution", "restaurant", "fashion", "refrigerator", "cooker", "metal", "techno", "apparel",
			"system", "electro", "software", "jewel", "confectioner", "cafe", "hospital", "bake", "electric", "cake", "medic", "cutting", "hotel", "eatery", "road", "cloth",
			"gallery", "university", "college", "school", "design", "studio", "energy", "diamond", "pearl", "gold", "court", "law", "alcohol", "beverage", "conditioner", "switch",
			"wire", "monitor", "computer", "stationary", "jeans", "shirt", "statistics", "footwear", "bike", "shoe", "paint", "mobile", "phone", "chair", "advertisement",
			"police", "party", "planner", "build", "realtor", "hardware", "beauty", "style", "book", "pens", "pencil", "ticket", "dentist", "doctor", "rubber", "plastic",
			"carpet", "appliance", "automobile", "institute", "salon", "furniture", "interior", "architect", "decorative", "bathroom", "telecom", "solar", "camera", "sport",
			"games", "accessories", "accessory", "garment", "television", "audio", "video", "pharma", "print", "textile", "fabric", "utensil", "cosmetic", "furnish", "fabricate",
			"zodiac", "drugs", "communication", "music", "artifact", "flooring", "banner", "billboard", "rent", "lease", "repair", "maintain", "marketing", "finance", "financial",
			"invest", "advisor", "consult", "train", "railyway", "travel", "tour", "resort", "hiring", "house", "detective", "carpentry", "carpenter", "translate", "translation",
			"translator", "aircraft", "aero", "bank", "research", "development", "analysis", "recruit", "insurance", "adventure", "fitness", "sewing", "safety", "construct",
			"hygiene", "education", "contract", "social", "publish", "logistic", "entertain", "catering", "civil", "association", "account", "estate", "legal", "organization",
			"administrative", "administration", "drone", "recreation", "mining", "professional", "scientific", "postal", "amusement", "park", "garden", "museum", "utilities",
			"land", "network", "residential", "community", "asset", "treat", "drain", "child", "diagnostic", "library", "broadcast", "host", "archive", "radio", "record",
			"ceramic", "toilet", "domestic", "exploration", "personal", "veterinary", "defense", "government", "animal", "store", "sightseeing", "coach", "polytechnic",
			"continential", "romantic", "maternity", "blood", "photo", "theatre", "cinema", "office", "decorator", "supermercado", "diner", "management", "boutique", "academy",
			"center", "shopping", "lounge", "professional", "event", "skill", "clinic", "bureau", "agency", "corporation", "brother", "enterprise", "inspect", "associate",
			"cable", "cooking", "cutlery", "security", "orphanage", "home", "delivery", "extinguish", "screen", "paper", "article", "health", "nutrition", "massage", "packaging",
			"decoration", "handmade", "closet", "faucet", "cisterns", "actuator", "taxi", "crane", "freight"));
	public static ArrayList<String> allowedWords = new ArrayList<String>(Arrays.asList("agri", "agro", "seed", "farm", "transport", "crop", "poultry", "meat", "paddy", "rice",
			"shipping", "wheat", "fish", "seafood"));

	public static void main(String[] args) {
		int i = 0;
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("users");
		try (Connection connection = PostgresDBUtil.getConnection("users");
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("select data->>'bizName', data->>'name', data->>'mobileNum' from master5");) {
			while (resultSet.next()) {
				if (resultSet.getString(1).length() <= 3) {
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(resultSet.getString(1));
					row.createCell(1).setCellValue(resultSet.getString(2));
					row.createCell(2).setCellValue(resultSet.getString(3));
					row.createCell(3).setCellValue("User flagged due to unreliable biz name");
					i++;
				} else {
					String result = filter(resultSet.getString(1));
					if (result != null) {
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(resultSet.getString(1));
						row.createCell(1).setCellValue(resultSet.getString(2));
						row.createCell(2).setCellValue(resultSet.getString(3));
						row.createCell(3).setCellValue(result);
						i++;
					}
				}
			}
			workbook.write(new FileOutputStream("/home/abhi/Desktop/USERS.xlsx"));
			System.out.println(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String filter(String data) {
		String matchAllowed = allowedWords.stream().filter(word -> data.toLowerCase().contains(word)).findFirst().orElse(null);
		String match = null;
		if (matchAllowed == null) {
			match = filters.stream().filter(word -> data.toLowerCase().contains(word)).findFirst().orElse(null);
		}
		return match;
	}
}