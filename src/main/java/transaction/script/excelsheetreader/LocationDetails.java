/*package transaction.script.excelsheetreader;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import transaction.script.constants.PlatformConstants;
import transaction.script.models.PlaceTo;
import transaction.script.platform.utils.HsqlDBUtil;

import com.google.gson.Gson;

public class LocationDetails {
	public static Connection conn = null;
	public static Gson gson = null;

	static {
		conn = HsqlDBUtil.dbConnection;
		gson = new Gson();
	}

	public static PlaceTo getLocationObject(String locationName, String countryId) throws SQLException {
		try {
			PlaceTo objPlaceTo = null;
			String locationId = "";
			String query = " SELECT LOCATION_ID FROM LOCATIONS  WHERE LOCATION_NAME = ? AND LOCATION_ID LIKE '" + countryId + "%'";
			System.out.println(query);
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, locationName);
			ResultSet objRS = ps.executeQuery();
			if (objRS.next()) {
				locationId = objRS.getString("LOCATION_ID");
				objPlaceTo = getlocationDetails(locationId);
				return objPlaceTo;
			} 
			 * else if (locationId == null || locationId.trim().isEmpty()) {
			 * List<PlaceTo> listOfPlaces = listOfPlacesForPickerSolr("null",
			 * countryId, locationName.split(",")[0], "0"); if
			 * (listOfPlaces.size() >= 1) { objPlaceTo = listOfPlaces.get(0);
			 * return objPlaceTo; }
			 
			else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// hsqldb method
	public static PlaceTo getlocationDetails(String locationId) throws SQLException {
		PlaceTo objLocation = new PlaceTo();
		String selectQuery = " SELECT CNTRY.COUNTRY_ID, CNTRY.COUNTRY_NAME, CNTRY.COUNTRY_SHORT_TWO, REG.REGION_ID, REG.REGION_NAME, CNTRY.TELECOM_COUNTRY_CODE, CNTRY.CURRENCY_NAME, STS.STATE_NAME, "
				+ " STS.STATE_ID, DIST.DISTRICT_NAME, DIST.DISTRICT_ID, LOC.LOCATION_ID,LOC.LOCATION_LONG,LOC.LOCATION_SHORT,LOC.COUNTRY_SHORT,LOC.LOCATION_CITY_ID,LOC.LOCATION_CATEGORY_ID,"
				+ " LOC.PINCODE,LOC.MARKET_ID,LOC.GEO_NAME_ID, (SELECT DISPLAY_NAME FROM LOCATIONS_LOCALE_NAMES LLN WHERE LLN.LOCATION_ID = LOC.LOCATION_ID AND LANGUAGE_ID = 1) AS LOCATION_NAME, "
				+ " LOC.LATITUDE, LOC.LONGITUDE FROM COUNTRIES CNTRY INNER JOIN REGIONS REG ON CNTRY.COUNTRY_ID = REG.COUNTRY_ID INNER JOIN STATES STS ON REG.REGION_ID = STS.REGION_ID "
				+ " INNER JOIN DISTRICTS DIST ON STS.STATE_ID = DIST.STATE_ID INNER JOIN LOCATIONS LOC ON DIST.DISTRICT_ID = LOC.DISTRICT_ID WHERE LOC.LOCATION_ID = ? ";
		PreparedStatement preparedStatement = conn.prepareStatement(selectQuery);
		preparedStatement.setString(1, locationId);
		ResultSet objResultSet = preparedStatement.executeQuery();
		if (objResultSet.next()) {
			objLocation.setPlaceId(objResultSet.getString("LOCATION_ID"));
			objLocation.setPlaceName(objResultSet.getString("LOCATION_NAME"));
			objLocation.setLatitude(objResultSet.getString("LATITUDE"));
			objLocation.setLongitude(objResultSet.getString("LONGITUDE"));
			objLocation.setDistrictId(objResultSet.getString("DISTRICT_ID"));
			objLocation.setDistrictName(objResultSet.getString("DISTRICT_NAME"));
			objLocation.setStateId(objResultSet.getString("STATE_ID"));
			objLocation.setStateName(objResultSet.getString("STATE_NAME"));
			objLocation.setRegionId(objResultSet.getString("REGION_ID"));
			objLocation.setRegionName(objResultSet.getString("REGION_NAME"));
			objLocation.setCountryId(objResultSet.getString("COUNTRY_ID"));
			objLocation.setCountryName(objResultSet.getString("COUNTRY_NAME"));
			objLocation.setCountryShortName(objResultSet.getString("country_short"));
			objLocation.setOpenerpCountryId(objResultSet.getString("COUNTRY_SHORT_TWO"));
			objLocation.setLocationLong(objResultSet.getString("location_long"));
			objLocation.setLocationShort(objResultSet.getString("location_short"));
			objLocation.setLocation_city_id(objResultSet.getString("LOCATION_CITY_ID"));
			objLocation.setLocation_category_id(objResultSet.getString("LOCATION_CATEGORY_ID"));
			objLocation.setPincode(objResultSet.getString("PINCODE"));
			objLocation.setMarket_id(objResultSet.getString("MARKET_ID"));
			objLocation.setGeo_name_id(objResultSet.getString("GEO_NAME_ID"));
		}
		preparedStatement.close();
		return objLocation;
	}

	// solr service
	public static List<PlaceTo> listOfPlacesForPickerSolr(String sessionId, String countryId, String subStrings, String categoryCode) {
		String comparator = "";
		String spellcheckWord = subStrings;
		try {
			comparator = subStrings.replaceAll(" ", " or ");
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
			spellcheckWord = java.net.URLEncoder.encode(spellcheckWord, "UTF-8");
			System.out.println(subStrings);
			subStrings = subStrings.replaceAll("\\+", "%20AND%20");
			System.out.println(subStrings);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		List<PlaceTo> placeLst = new ArrayList<PlaceTo>();
		Response clientResponse = null;
		String URL = PlatformConstants.COMMONDATA_SEARCH + "(filter:location%20AND%20countryId:" + countryId + ")%20AND%20" + subStrings
				+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs&start=0&rows=5000";
		WebTarget webTarget = HsqlDBUtil.client().target(URL);
		Invocation.Builder invocationBuilder = webTarget.request();
		clientResponse = invocationBuilder.get();
		String output = clientResponse.readEntity(String.class);
		JSONObject objJson = new JSONObject(output);
		System.out.println(objJson);
		JSONObject responsedata = objJson.getJSONObject("response");
		String suggestedQuery = null;
		if (responsedata.get("numFound").toString().equals("0")) {
			JSONObject temp = objJson.getJSONObject("spellcheck");
			JSONObject misSpellArray = null;
			JSONArray queryAndSuggestion = null;
			JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
			for (int i = 0; i < suggestionsArray.length(); i++) {
				if (suggestionsArray.get(i).toString().equalsIgnoreCase(comparator)) {
					misSpellArray = new JSONObject(suggestionsArray.get(i + 1).toString());
					queryAndSuggestion = misSpellArray.getJSONArray("suggestion");
				}
			}
			if (queryAndSuggestion != null) {
				suggestedQuery = subStrings = queryAndSuggestion.get(0).toString();
			} else {
				return placeLst;
			}
			subStrings = subStrings.replaceAll(" ", "%20AND%20");
			URL = PlatformConstants.COMMONDATA_SEARCH + "(filter:location%20AND%20countryId:" + countryId + ")%20AND%20" + subStrings
					+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName&start=0&rows=5000";
			System.out.println("URL IS " + URL);
			webTarget = HsqlDBUtil.client().target(URL);
			invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			output = clientResponse.readEntity(String.class);
			objJson = new JSONObject(output);

			objJson = new JSONObject(output);
			try {
				if (objJson.has("response")) {
					responsedata = objJson.getJSONObject("response");
				} else {
					return placeLst;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		String result = responsedata.toString();
		JSONObject obj = new JSONObject(result);
		Object test = obj.get("docs");
		JSONArray array = new JSONArray(test.toString());
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj1 = new JSONObject(array.get(i).toString());
			PlaceTo objPlaceTo = gson.fromJson(obj1.get("_json_").toString(), PlaceTo.class);
			placeLst.add(objPlaceTo);
		}
		Collections.sort(placeLst, new Comparator<PlaceTo>() {
			@Override
			public int compare(PlaceTo o1, PlaceTo o2) {
				String placeName1 = o1.getPlaceName().toUpperCase();
				String placeName2 = o2.getPlaceName().toUpperCase();
				return placeName1.compareTo(placeName2);
			}
		});
		return placeLst;
	}
}*/