/*package transaction.script.excelsheetreader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import transaction.script.models.PlaceTo;
import transaction.script.models.ProductTo;
import transaction.script.models.RegistrationTo;

public class ReadExcelsheet {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReadExcelsheet.class);
	public static Gson gson = new Gson();

	
	 * public static void readExcelSheet(String excelPath) throws Exception {
	 * XSSFWorkbook workBook; DataFormatter df = new DataFormatter(); workBook =
	 * new XSSFWorkbook(excelPath); XSSFSheet preCrtDataSheet =
	 * workBook.getSheetAt(0); RegistrationTo objRegistrationTo = new
	 * RegistrationTo(); for (int i = 0; i <= preCrtDataSheet.getLastRowNum();
	 * i++) { Row row = preCrtDataSheet.getRow(i);
	 * objRegistrationTo.setName(df.formatCellValue(row.getCell(0)));
	 * objRegistrationTo.setBusinessName(df.formatCellValue(row.getCell(1))); if
	 * (df.formatCellValue(row.getCell(2))) { // mobileno }
	 * objRegistrationTo.setMobileNumber("");
	 * objRegistrationTo.setMobileTelecomCode("");
	 * objRegistrationTo.setBusinessTypeName
	 * (df.formatCellValue(row.getCell(3)));
	 * objRegistrationTo.setBusinessTypeId("");
	 * if(df.formatCellValue(row.getCell(4))){ //location }
	 * objRegistrationTo.setLocationId(""); objRegistrationTo.setLocationTo("");
	 * objRegistrationTo.setApmcTo(""); if(df.formatCellValue(row.getCell(5))){
	 * //products } objRegistrationTo.setProductIds("");
	 * objRegistrationTo.setLstOfProducts("");
	 * objRegistrationTo.setLstBizInterest(""); } }
	 
	public static RegistrationTo settingRegistrationTo(HashMap<String, String> excelRecord) throws Exception {
		RegistrationTo objRegistrationTo = new RegistrationTo();
		ArrayList<ProductTo> productTolist = new ArrayList<ProductTo>();
		Set<ProductTo> productSearchset = new TreeSet<ProductTo>();
		ArrayList<PlaceTo> locationTolist = new ArrayList<PlaceTo>();
		PlaceTo objPlaceTo = null;
		objRegistrationTo.setName(excelRecord.get("NAME"));
		objRegistrationTo.setBusinessName(excelRecord.get("BUSINESSNAME"));
		objRegistrationTo.setBusinessTypeName(excelRecord.get("BIZTYPE"));
		objRegistrationTo.setBusinessTypeId(excelRecord.get("BUSINESSTYPEID"));
		String countryName = excelRecord.get("COUNTRYNAME");
		String countryId = excelRecord.get("COUNTRYID");
		String telecomeCode = excelRecord.get("MOBILETELECOMECODE");
		objRegistrationTo.setLocationId(excelRecord.get("LOCATIONNAME"));
		if (objRegistrationTo.getLocationId() != null && !objRegistrationTo.getLocationId().isEmpty()) {
			objPlaceTo = LocationDetails.getLocationObject(objRegistrationTo.getLocationId(),countryId);
		}
		locationTolist.add(objPlaceTo);
		objRegistrationTo.setApmcTo(objPlaceTo);
		objRegistrationTo.setLocationTo(objPlaceTo);
		objRegistrationTo.setLstBizInterest(locationTolist);
		objRegistrationTo.setLicenseNo(excelRecord.get("LIECENSENO"));
		objRegistrationTo.setProductIds(excelRecord.get("PRODUCTNAMES"));
		System.out.println("@@@@@@"+objRegistrationTo.getProductIds());
		if (objRegistrationTo.getProductIds() != null && !objRegistrationTo.getProductIds().isEmpty()) {
			productTolist = (ArrayList<ProductTo>) ProductDetails.getProductDetails(objRegistrationTo.getProductIds(), countryId);
			System.out.println("@@@@@@@@@@@@");
		}
		if (productTolist.size() == 0) {
			productSearchset = ProductDetails.ProductsSearch(objRegistrationTo.getProductIds(), countryId, objRegistrationTo.getLocationId().substring(0, 7), "0");
			productTolist.addAll(productSearchset);
		}
		objRegistrationTo.setLstOfProducts(productTolist);
		objRegistrationTo.setMobileNumber(excelRecord.get("MOBILENUMBER"));
		objRegistrationTo.setMobileTelecomCode(telecomeCode);
		System.out.println("!!!!!!!!!!!!!!!!" + gson.toJson(objRegistrationTo));
		return objRegistrationTo;
	}

	public static void main(String[] args) throws Exception {
		HashMap<String, String> obj1 = new HashMap<String, String>();
		obj1.put("NAME", "aaa");
		obj1.put("BUSINESSNAME", "aaa");
		obj1.put("BIZTYPE", "AgriTrader");
		obj1.put("BUSINESSTYPEID", "BT000000");
		obj1.put("COUNTRYNAME", "INDIA");
		obj1.put("COUNTRYID", "101");
		obj1.put("MOBILETELECOMECODE", "+91");
		obj1.put("LOCATIONNAME", "Akithi");
		obj1.put("LIECENSENO", "789");
		obj1.put("PRODUCTNAMES", "Paddy,Maize");
		obj1.put("MOBILENUMBER", "7896541237");
		settingRegistrationTo(obj1);
	}
}
*/