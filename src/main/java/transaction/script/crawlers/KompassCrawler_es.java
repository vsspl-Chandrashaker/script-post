package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class KompassCrawler_es {
	private static Map<String, String> cookieDetails = new HashMap<String, String>();

	public static void main(String[] args) {
		String baseUrl = "http://in.kompass.com";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0, count = 0;
		boolean shouldIRun = true;
		System.out.println("Hulk smash...");
		try {
			getNewCookieDetails();
			for (int i = 90; shouldIRun && i < 180; i++) {
				boolean shouldIStop = false;
				if (shouldIStop)
					break;
				try {
					Response linksPageResponse = null;
					for (int j = 0; j < 3; j++) {
						try {
							linksPageResponse = Jsoup.connect("http://in.kompass.com/searchCompanies/scroll?pageNbre=" + i).cookies(cookieDetails).timeout(60000)
									.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").execute();
						} catch (HttpStatusException e) {
							if (e.toString().contains("Status=503") && j == 2) {
								System.out.println("Blocked....");
								shouldIStop = true;
							}
						} catch (Exception e1) {
							System.out.println("trying main links page again....");
							e1.printStackTrace();
						}
					}
					if (linksPageResponse == null)
						continue;
					if (count >= 3954) {
						shouldIRun = false;
						continue;
					}
					TimeUnit.SECONDS.sleep(5);
					Element linksPage = linksPageResponse.parse().getElementById("resultatDivId");
					for (Element element : linksPage.getElementsByClass("details")) {
						count++;
						String bizName = "", address = "", mobile = "", summary = "", fax = "", website = "", userName = "", activities = "";
						String subLinkUrl = baseUrl + element.getElementsByTag("h2").get(0).getElementsByTag("a").attr("href");
						Response subLinkPageResponse = null;
						for (int j = 0; j < 3; j++) {
							try {
								subLinkPageResponse = Jsoup.connect(subLinkUrl).cookies(cookieDetails)
										.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000)
										.execute();
								break;
							} catch (HttpStatusException e) {
								if (e.toString().contains("Status=503") && j == 2) {
									System.out.println("Blocked....");
									shouldIStop = true;
									shouldIRun = false;
								}
							} catch (Exception e) {
								System.out.println("trying again....");
								e.printStackTrace();
							}
						}
						if (subLinkPageResponse == null)
							continue;
						Document subLinkDocument = subLinkPageResponse.parse();
						if (!subLinkDocument.getElementsByTag("h1").isEmpty())
							bizName = subLinkDocument.getElementsByTag("h1").text();
						if (!subLinkDocument.getElementsByClass("infos").isEmpty())
							address = subLinkDocument.getElementsByClass("infos").text();
						if (!subLinkDocument.getElementsByClass("coordinatesPhoneMail").isEmpty())
							mobile = subLinkDocument.getElementsByClass("coordinatesPhoneMail").get(0).getElementsByTag("input").attr("value");
						if (!subLinkDocument.getElementsByClass("description").isEmpty())
							summary = subLinkDocument.getElementsByClass("description").get(0).getElementsByClass("presentation").text();
						for (Element generalData : subLinkDocument.getElementsByClass("general").get(0).getElementsByTag("li")) {
							if (generalData.text().contains("Fax")) {
								fax = generalData.getElementsByTag("p").get(0).nextElementSibling().text();
							} else if (generalData.text().contains("Website")) {
								for (Element link : generalData.getElementsByTag("a")) {
									if (website.isEmpty())
										website = link.attr("href");
									else
										website = website + "," + link.attr("href");
								}
							}
						}
						for (Element name : subLinkDocument.getElementsByClass("TabContacts")) {
							userName = name.getElementsByClass("name").text();
							break;
						}
						activities = subLinkDocument.getElementById("activities").text();
						Row row = sheet.createRow(rowNumber);
						rowNumber++;
						row.createCell(0).setCellValue(userName);
						row.createCell(1).setCellValue(bizName);
						row.createCell(2).setCellValue(address);
						row.createCell(3).setCellValue(mobile);
						row.createCell(4).setCellValue(website);
						row.createCell(5).setCellValue(fax);
						row.createCell(6).setCellValue(summary);
						row.createCell(7).setCellValue(activities);
						TimeUnit.MILLISECONDS.sleep(250);
						if (rowNumber % 50 == 0) {
							System.out.println("Hulk Thunderclap....");
							getNewCookieDetails();
						}
						System.out.println("Currently crawled... " + count);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Might be a major unhandled exception so writing data to sheets");
		} finally {
			if (workbook != null) {
				try (FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\Ani\\Desktop\\test2.xlsx"))) {
					workbook.write(outputStream);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Hulk at peace....\u263A \u263A");
	}

	private static void getNewCookieDetails() {
		Response response = null;
		String javaSessionId = "", language = "", routeId = "";
		try {
			response = Jsoup
					.connect("http://in.kompass.com/searchCompanies?acClassif=&localizationCode=UA&localizationLabel=Ukraine&localizationType=country&text=food&searchType=ALL")
					.timeout(60000).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").execute();
			javaSessionId = response.cookie("JSESSIONID");
			language = response.cookie("_k_cty_lang");
			routeId = response.cookie("ROUTEID");
			cookieDetails.put("JSESSIONID", javaSessionId);
			cookieDetails.put("_k_cty_lang", language);
			cookieDetails.put("ROUTEID", routeId);
			//System.out.println(new Gson().toJson(cookieDetails));
		} catch (Exception e) {
			e.printStackTrace();
			if (e.toString().contains("Status=503")) {
				System.out.println("Blocked");
			}
			if (cookieDetails.isEmpty()) {
				System.out.println("Unable to initialise...");
				System.exit(1);
			} else {
				System.out.println("Re using the same cookie...");
			}
		}
	}
}