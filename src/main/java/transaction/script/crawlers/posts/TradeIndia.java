package transaction.script.crawlers.posts;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TradeIndia {
	public static String doamin = "http://www.tradeindia.com";

	public static void main(String[] args) throws Exception {

		try {
			System.out.println("dfgfdghdfhdfh");
			for (int pageNum = 1; pageNum < 4; pageNum++) {
				Thread.sleep(1000);
				FileInputStream fs = new FileInputStream("/home/vasudhaika/posttest.xlsx");
				Workbook workbook = new XSSFWorkbook(fs);
				Sheet sheet = workbook.getSheetAt(0);
				int rowNumber = sheet.getLastRowNum()+1;
				Document doc = null;
				if (pageNum == 1) {
					doc = Jsoup.connect("https://www.tradeindia.com/Seller/Agriculture/Agriculture-Product-Stocks/?nationality=NonIndian&search_form_id=18")
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
				} else {
					doc = Jsoup.connect("https://www.tradeindia.com/Seller/Agriculture/Agriculture-Product-Stocks/?num_data=45&search_form_id=18&page_no=" + pageNum + "&set=1&nationality=NonIndian")
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
				}
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" + pageNum);
				Elements text1 = doc.getElementsByClass("co_namel");
				Document linkDocument = null;
				int index = 0;
				HashSet<String> sublinks = new HashSet<String>();
				for (Element element : text1) {
					Elements text = text1.get(index).getElementsByTag("a");
					String subLink = text.attr("href");
					sublinks.add(subLink);
					index++;
				}
				System.out.println(sublinks.size());
				for (String subLink : sublinks) {
					ArrayList<String> listStr = new ArrayList<String>();
					try {
						linkDocument = Jsoup.connect(doamin + subLink).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
								.timeout(60000).get();
						Elements mainText = linkDocument.getElementsByClass("text-f12");
						Elements websiteText = linkDocument.getElementsByClass("blue-f12");
						int textindex = 0;
						int textendIndex = textindex + 8;
						for (int i = 0; i < mainText.size(); i++) {
							Element main = mainText.get(i);
							if (main.text().startsWith("Business Type") && main.text().length() <= 20) {
								textindex = 1;
							}
							if (main.text().startsWith("Import Export Code") || main.text().startsWith("Member Affiliates") || main.text().startsWith("Year Established")
									|| main.text().startsWith("Standard") || main.text().startsWith("Company Branches")) {
								i++;
								continue;
							} else if (!main.text().startsWith("Website")) {
								if (textindex > 0 && textindex <= textendIndex) {
									listStr.add(main.text());
									textindex++;
								}
							}
						}
						for (Element website : websiteText) {
							if (website.text().startsWith("http") || website.text().startsWith("https")) {
								listStr.add(website.text());
							}
						}

						System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						for (String str : listStr) {
							System.out.println("ssssssssssssssssssssssssssssssssssssssss" + str);
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					rowNumber = writingExcel(listStr, workbook, sheet, rowNumber);
				}
				FileOutputStream outputStream = new FileOutputStream("/home/vasudhaika/posttest.xlsx");
				workbook.write(outputStream);
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" + pageNum);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

	public static int writingExcel(ArrayList<String> list, Workbook workbook, Sheet sheet, int rowNumber) {
		System.out.println(rowNumber);
		Row row = sheet.createRow(rowNumber);
		try {
			row.createCell(0).setCellValue(list.get(1));
			row.createCell(1).setCellValue(list.get(3));
			row.createCell(2).setCellValue(list.get(4));
			row.createCell(3).setCellValue(list.get(5));
			row.createCell(4).setCellValue(list.get(6));
			for (int i = 7; i < list.size(); i++) {
				if (list.get(i).startsWith("http://") || list.get(i).startsWith("https://")) {
					row.createCell(5).setCellValue(list.get(i));
				} else if (list.get(i).startsWith("+") || list.get(i).matches("[0-9]+")) {
					row.createCell(6).setCellValue(list.get(i));
				}
			}
			row.createCell(7).setCellValue("Sell");
			return rowNumber + 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			return rowNumber;
		}
	}
}
