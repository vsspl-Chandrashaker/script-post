package transaction.script.crawlers.posts;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Go4worldBusinessPostsCrawler {
	//54buyers+219suppliers

	public static void main(String[] args) {
		String baseUrl = "http://www.go4worldbusiness.com";
		String dataLocationHelper = "GOLD Premium members can contact unlimited buyers and SILVER Premium members can contact 50 buyers per week";
		Document document = null;
		for (int i = 55, j = 1; i <= 54 || j <= 1; i++, j++) {
			try {
				document = Jsoup
						.connect(
								"http://www.go4worldbusiness.com/find/worldwide/buyers/agriculture-and-food.html?countryFilter%5B%5D=Indonesia&pg_buyers=" + i + "&pg_suppliers="
										+ j + "&matchToCatgListOnly=0").timeout(60000).get();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (i <= 24) {
				Elements buyerLinksPage = document.getElementsByClass("tab-content").get(0).getElementById("Buyers").getElementById("Buyers ").getElementsByClass("search-results");
				for (Element element : buyerLinksPage) {
					String subLinkURL = element.getElementsByTag("a").get(0).attr("href");
					if (subLinkURL.startsWith("/member/")) {
						continue;
					} else {
						String subjectLine = "", message = "", product = "", quantity = "", postCreatedDate = "";
						boolean verified = false;
						Document subLinkDocument = null;
						try {
							subLinkDocument = Jsoup.connect(baseUrl + subLinkURL).timeout(60000).get();
						} catch (IOException e) {
							e.printStackTrace();
						}
						subjectLine = subLinkDocument.getElementsByTag("h1").text();
						product = subjectLine.substring(subjectLine.indexOf(":") + 1).trim();
						System.out.println("subject is ::" + subjectLine);
						System.out.println("product is ::" + product);
						for (Element details : subLinkDocument.getElementsByClass("borderJUNIOR").get(0).getElementsByTag("small")) {
							if (details.text().trim().equals("VERIFIED")) {
								verified = true;
							} else {
								SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yy");
								try {
									postCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(format.parse(details.text().trim()));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
						}
						System.out.println("verified ::" + verified);
						System.out.println("post created on ::" + postCreatedDate);
						for (Element rowElement : subLinkDocument.getElementsByClass("row")) {
							if (rowElement.text().contains(dataLocationHelper)) {
								String temp = rowElement.text();
								message = temp.substring(0, temp.indexOf(dataLocationHelper));
								System.out.println("message is ::" + message);
								for (String data : rowElement.html().split("<br>")) {
									if (data.contains(" Quantity Required : ")) {
										quantity = data.substring(data.indexOf(": ") + 2).trim();
										System.out.println("quantity is ::" + quantity);
									}
								}
							}
						}
					}
				}
			}
			if (j <= 218) {
				Elements suppliersLinksPage = document.getElementsByClass("tab-content").get(0).getElementById("Suppliers").getElementById("Suppliers ")
						.getElementsByClass("search-results");
				for (Element suppliersLinks : suppliersLinksPage) {
					String subLinkURL = suppliersLinks.getElementsByTag("a").get(0).attr("href");
					System.out.println(baseUrl + subLinkURL);
					String subjectLine = "", message = "", product = "", quantity = "", postCreatedDate = "";
					boolean verified = false;
					Document subLinkDocument = null;
					try {
						subLinkDocument = Jsoup.connect(baseUrl + subLinkURL).timeout(60000).get();
					} catch (IOException e) {
						e.printStackTrace();
					}
					subjectLine = subLinkDocument.getElementsByTag("h1").text();
					product = subjectLine.substring(subjectLine.indexOf(":") + 1).trim();
					System.out.println("subject is ::" + subjectLine);
					System.out.println("product is ::" + product);
					if (subLinkDocument.getElementsByClass("borderJUNIOR").size() > 0) {
						for (Element details : subLinkDocument.getElementsByClass("borderJUNIOR").get(0).getElementsByTag("small")) {
							if (details.text().trim().equals("VERIFIED")) {
								verified = true;
							} else {
								SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yy");
								try {
									postCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(format.parse(details.text().trim()));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
						}
					} else if (subLinkDocument.getElementsByClass("borderSILVER").size() > 0) {
						for (Element details : subLinkDocument.getElementsByClass("borderSILVER").get(0).getElementsByTag("small")) {
							if (details.text().trim().equals("VERIFIED")) {
								verified = true;
							} else {
								SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yy");
								try {
									postCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(format.parse(details.text().trim()));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
						}
					} else if (subLinkDocument.getElementsByClass("borderGOLD").size() > 0) {
						for (Element details : subLinkDocument.getElementsByClass("borderGOLD").get(0).getElementsByTag("small")) {
							if (details.text().trim().equals("VERIFIED")) {
								verified = true;
							} else {
								SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yy");
								try {
									postCreatedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(format.parse(details.text().trim()));
								} catch (ParseException e) {
									e.printStackTrace();
								}
							}
						}
					}
					System.out.println("verified ::" + verified);
					System.out.println("post created on ::" + postCreatedDate);
					System.out.println("\n\n\n");
				}
			}
		}
	}
}