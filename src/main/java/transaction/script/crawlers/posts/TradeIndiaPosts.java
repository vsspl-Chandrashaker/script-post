package transaction.script.crawlers.posts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

import transaction.script.platform.utils.PostgresDBUtil;

public class TradeIndiaPosts {
	public static String domain = "https://www.tradeindia.com";
	public static Connection conntestpost = PostgresDBUtil.getConnection("users");
	public static String insertQuery = "INSERT INTO post_crawl(product_name, data)VALUES(?,CAST(? AS jsonb))";
	public static PreparedStatement pst = null;
	static Gson gson = new Gson();

	public static void main(String[] args) {
		try {
			System.out.println("dfgfdghdfhdfh");
			Document doc = Jsoup.connect("https://www.tradeindia.com/TradeLeads/buy/Agriculture/")
					.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
			Elements products = doc.getElementsByClass("categorylist");
			Document linkDocument = null;
			int index = 0;
			HashSet<String> sublinks = new HashSet<String>();
			for (Element product : products) {
				String subLink = product.attr("href");
				sublinks.add(subLink);
				index++;
			}
			System.out.println(sublinks.size());
			for (String subLink : sublinks) {

				try {
					linkDocument = Jsoup.connect(domain + subLink).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000)
							.get();
					Elements postProducts = linkDocument.getElementsByClass("co_namel");
					Document postDocument = null;
					for (Element postProduct : postProducts) {
						String subject = "", postDate = "", expiryDate = "", description = "", companyName = "", address = "", postProductName = "", mobileNo = "", website = "";
						String postLink = postProduct.attr("href");
						postDocument = Jsoup.connect(domain + postLink).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
								.timeout(60000).get();
						Elements headings = postDocument.getElementsByTag("strong");
						subject = headings.get(3).text();
						postProductName = subject;
						companyName = headings.get(6).text();
						Elements dates = postDocument.getElementsByClass("black");
						postDate = dates.get(0).text().split(":")[1];
						expiryDate = dates.get(1).text().split(":")[1];
						description = dates.get(2).text();
						Elements addressStr = postDocument.getElementsByClass("black12");
						address = addressStr.get(0).text();
						PostToClass objPostToClass = settingPostTo(domain + postLink, mobileNo, companyName, subject, description, postProductName, address, postDate, expiryDate, website);
						int result = insertionDB(objPostToClass);
						if(result>0){
							System.out.println("success");
						}
						else{
							System.out.println("fail");	
						}
					}
					System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static PostToClass settingPostTo(String postLink, String mobileNo, String companyName, String subject, String description, String postProductname, String address, String postDate,
			String expairyDate, String website) {
		PostToClass objPostToClass = new PostToClass();
		objPostToClass.setAddress(address);
		objPostToClass.setCompanyName(companyName);
		objPostToClass.setDescription(description);
		objPostToClass.setExpiaryDate(expairyDate);
		objPostToClass.setMobileNo(mobileNo);
		objPostToClass.setPostDate(postDate);
		objPostToClass.setProduct(postProductname);
		objPostToClass.setSubject(subject);
		objPostToClass.setLink(postLink);
		objPostToClass.setWebsite(website);
		return objPostToClass;
	}

	public static int insertionDB(PostToClass objPostToClass) {
		try {
			pst = conntestpost.prepareStatement(insertQuery);
			pst.setString(1, objPostToClass.getProduct());
			String data = gson.toJson(objPostToClass);
			pst.setString(2, data);
			pst.executeUpdate();
			return 1;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}
