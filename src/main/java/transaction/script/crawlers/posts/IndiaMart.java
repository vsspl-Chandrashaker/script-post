package transaction.script.crawlers.posts;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

public class IndiaMart {
	public static String domain = "http://dir.indiamart.com";

	static Gson gson = new Gson();

	public static void main(String[] args) {
		try {
			System.out.println("dfgfdghdfhdfh");
			Document doc = null;
			for (int i = 0; i < 3; i++) {
				try {
					doc = Jsoup.connect("http://dir.indiamart.com/industry/agro-poultry-dairy.html")
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(120000).get();
					if (doc != null)
						break;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					TimeUnit.MILLISECONDS.sleep(100);
				}
			}
			Elements mainCatagories = doc.getElementsByTag("li");
			ArrayList<String> categoriesLink = new ArrayList<String>();
			for (int i = 26; i < mainCatagories.size() - 5; i++) {
				Element category = mainCatagories.get(i);
				String categoryLink = category.getElementsByTag("a").get(0).attr("href");
				System.out.println("######################################################" + categoryLink);
				categoriesLink.add(categoryLink);
			}
			for (int i = 0; i < categoriesLink.size(); i++) {
				Document categoryDocument = null;
				for (int j = 0; j < 3; j++) {
					try {
						categoryDocument = Jsoup.connect(domain + categoriesLink.get(i))
								.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
						if (categoryDocument != null)
							break;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						TimeUnit.MILLISECONDS.sleep(100);
						e.printStackTrace();
					}
				}
				System.out.println("hai");
				Elements products = categoryDocument.getElementsByClass("box");
				Document postDocument = null;
				for (Element product : products) {
					String productLink = product.getElementsByTag("a").get(0).attr("href");
					System.out.println(productLink);
					for (int k = 0; k < 3; k++) {
					try {
						postDocument = Jsoup.connect(domain + productLink).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
								.timeout(60000).get();
						if (postDocument != null)
							break;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						TimeUnit.MILLISECONDS.sleep(100);
					}
					}
					System.out.println("hello");
					// Thread.sleep(6000);
					Elements postProducts = postDocument.getElementsByAttribute("data-dispid");
					for (Element postproduct : postProducts) {
						String subject = "", postDate = "", expiryDate = "", description = "", companyName = "", address = "", postProductName = "", mobileNo = "", website = "";
						companyName = postproduct.getElementsByClass("lcname").text();
						postProductName = postproduct.getElementsByClass("ldf").text();
						description = postproduct.getElementsByClass("lstw").text();
						address = postproduct.getElementsByClass("clg").text();
						mobileNo = postproduct.getElementsByClass("hlb").text();
						PostToClass objPostToClass = TradeIndiaPosts.settingPostTo(domain + productLink, mobileNo, companyName, subject, description, postProductName, address, postDate, expiryDate,
								website);
						objPostToClass.setPostType("sell");
						int result = TradeIndiaPosts.insertionDB(objPostToClass);
						if (result > 0) {
							System.out.println("success");
						} else {
							System.out.println("fail");
							System.out.println(gson.toJson(objPostToClass));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
