package transaction.script.excelSheet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GoogleSearchJava {

	public static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";

	public static void main(String[] args) throws IOException {
		// Extracting data from excel sheet
		HashSet<String> urls = null;
		Set<String> ignorelist = new HashSet<String>();
		ignorelist.add("https://www.youtube.com/?gl=IN");
		ignorelist.add("https://www.facebook.com/");
		ignorelist.add("https://twitter.com/login");
		ignorelist.add("book");

		String FilePath = "/home/vasudhaika/test.xlsx";
		FileInputStream fs = new FileInputStream(FilePath);
		Workbook workbook = new XSSFWorkbook(fs);
		Sheet firstSheet = workbook.getSheetAt(0);
		int rowNum = firstSheet.getLastRowNum() + 1;
		System.out.println("rowNum" + rowNum);
		int colNum = firstSheet.getRow(0).getLastCellNum();
		System.out.println("colNum" + colNum);
		String[][] data = new String[rowNum][colNum];
		System.out.println("data" + data);
		for (int i = 0; i < rowNum; i++) {
			ArrayList<String> nameUrls = new ArrayList<String>();
			ArrayList<String> emailUrls = new ArrayList<String>();
			Row row = firstSheet.getRow(i);
			boolean isIgnoredurl = false;
			for (int j = 0; j < colNum; j++) {
				Cell cell = row.getCell(j);
				System.out.println("j value is " + j);
				String value = cell.toString();
				data[i][j] = value;
				String searchURL = GOOGLE_SEARCH_URL + "?q=" + value + "&num=" + 20;
				// without proper User-Agent, we will get 403 error
				Document doc = Jsoup.connect(searchURL).userAgent("Mozilla/5.0").get();
				// If google search results HTML change the <h3 class="r" to <h3
				// class="r1"
				// we need to change below accordingly
				Elements results = doc.select("h3.r > a");
				urls = new HashSet<String>();
				List<String> listFinal = new ArrayList<String>();
				for (Element result : results) {
					String linkHref = result.attr("href");
					String linkhref1 = linkHref.substring(7, linkHref.indexOf("&")).trim();
					System.out.println(linkhref1);
					for (String ignoredurl : ignorelist) {
						if (linkhref1.contains(ignoredurl)) {
							isIgnoredurl = true;
							break;
						}
					}
					if (isIgnoredurl) {
						isIgnoredurl = false;
					} else {
						listFinal.add(linkhref1);
					}
				}
				if (j == 0) {
					nameUrls.addAll(listFinal);
					System.out.println(listFinal.size());
					System.out.println(nameUrls.size());
				} else {
					emailUrls.addAll(listFinal);
				}
			}
			
		}
		
	}
}
