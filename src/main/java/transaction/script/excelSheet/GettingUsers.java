package transaction.script.excelSheet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.hsqldb.rights.User;

import com.google.gson.Gson;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.UserBusinessDetails;
import transaction.script.models.UserProfileDataTo;
import transaction.script.platform.utils.PostgresDBUtil;

public class GettingUsers {
	static Gson gson = new Gson();

	public static void main(String args[]) {
		try {
			ArrayList<HashMap<String, MasterCollectionTo>> masterUsers = new ArrayList<HashMap<String, MasterCollectionTo>>();
			Connection conntestUsers = PostgresDBUtil.getConnection("users");
			Connection conntestInAactive = PostgresDBUtil.getConnection("profiles");
			Connection conntestActive = PostgresDBUtil.getConnection("appdatabackup");
			String inActiveQuery = "SELECT * FROM inactive_profiles";
			String activeQuery = "SELECT * FROM active_profiles";
			String insertQuery = "INSERT INTO dev_master(mobile_number, data)VALUES(?,CAST(? AS jsonb))";
			Statement stmtInactive = conntestInAactive.createStatement();
			Statement stmtActive = conntestActive.createStatement();
			PreparedStatement pst = conntestUsers.prepareStatement(insertQuery);
			ResultSet rsInactive = stmtInactive.executeQuery(inActiveQuery);
			while (rsInactive.next()) {
				String id = rsInactive.getString("id");
				String data = rsInactive.getString("data");
				UserProfileDataTo userProfileData = gson.fromJson(data, UserProfileDataTo.class);
				MasterCollectionTo user =regToMaster(userProfileData);
				HashMap<String, MasterCollectionTo> userMap = new HashMap<String, MasterCollectionTo>();
				userMap.put(id, user);
				masterUsers.add(userMap);
			}
			System.out.println(masterUsers.size());
			ResultSet rsActive = stmtActive.executeQuery(activeQuery);
			while (rsActive.next()) {
				String id = rsActive.getString("id");
				String data = rsActive.getString("data");
				UserProfileDataTo userProfileData = gson.fromJson(data, UserProfileDataTo.class);
				MasterCollectionTo user =regToMaster(userProfileData);
				HashMap<String, MasterCollectionTo> userMap = new HashMap<String, MasterCollectionTo>();
				userMap.put(id, user);
				masterUsers.add(userMap);
			}
			System.out.println(masterUsers.size());
			for (int i = 0; i < masterUsers.size(); i++) {
				HashMap<String, MasterCollectionTo> userMap = new HashMap<String, MasterCollectionTo>();
				userMap=masterUsers.get(i);
				for(String id:userMap.keySet()){
					//pst.setString(1, id);
					
					MasterCollectionTo user=userMap.get(id);
					pst.setString(1, user.getMobileNum());
					String data=gson.toJson(user);
					System.out.println(data);
					pst.setString(2, data);
				}
				//pst.addBatch();
				pst.executeUpdate();
			}
			//pst.executeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static MasterCollectionTo regToMaster(UserProfileDataTo objUserProfileDataTo){
		MasterCollectionTo objMasterCollectionTo=new MasterCollectionTo();
		UserBusinessDetails userdetail=objUserProfileDataTo.getLstOfUserBusinessDetailsInfo().get(0);
		objMasterCollectionTo.setBizId(userdetail.getBusinessTypeId());
		objMasterCollectionTo.setBizName(userdetail.getBusinessName());
		objMasterCollectionTo.setBizTypeId(userdetail.getBusinessTypeId());
		objMasterCollectionTo.setCountryCode(userdetail.getLocationTo().getCountryId());
		objMasterCollectionTo.setLocationId(userdetail.getLocationTo().getPlaceId());
		objMasterCollectionTo.setMobileNum(objUserProfileDataTo.getMobileNo());
		objMasterCollectionTo.setName(objUserProfileDataTo.getFirstName());
		objMasterCollectionTo.setProfileId(objUserProfileDataTo.getProfileKey());
		objMasterCollectionTo.setStatus(AppConstants.ACTIVE);
		return objMasterCollectionTo;
	}
}
