package transaction.script.excelSheet.validators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.constants.PlatformConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.PlaceTo;
import transaction.script.models.ProductTo;
import transaction.script.models.RegistrationTo;
import transaction.script.models.UnitDeatilsTo;
import transaction.script.platform.utils.HsqlDBUtil;
import transaction.script.platform.utils.MongoDBUtil;
import transaction.script.platform.utils.SearchProductTo;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;

public class CreateDemoAccounts extends Thread {
	public static final Logger LOGGER = LoggerFactory.getLogger(CreateDemoAccounts.class);
	static Client client = ClientBuilder.newClient();
	static DB db = null;
	static DBCollection dumpingCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static Connection hsqlConnection = null;
	static volatile HashMap<String, ProductTo> productsMap = new HashMap<String, ProductTo>();
	static volatile HashMap<String, PlaceTo> locationsMap = new HashMap<String, PlaceTo>();
	static volatile Set<String> processedMobileNumbers;
	static volatile Long currentDummyMobileNumber;
	static Pattern pattern = Pattern.compile("[^a-z A-Z \\s]*");
	static volatile HashMap<String, String> sheetDetails = new HashMap<String, String>();
	static volatile boolean shouldICleanUpAndStop = false;
	static {
		try {
			client.property(ClientProperties.CONNECT_TIMEOUT, 60000);
			client.property(ClientProperties.READ_TIMEOUT, 60000);
			hsqlConnection = HsqlDBUtil.dbConnection;
			db = MongoDBUtil.getConnection();
			dumpingCollection = db.getCollection("DemoCollection");//EligibleSept28test
			masterCollection = db.getCollection("masterCollectionOCT1");//abhimastertest
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise scripts");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args1) {
		System.out.println("Enter sheet name with location::");
		Scanner scanner = new Scanner(System.in);
		CreateDemoAccounts classObj = new CreateDemoAccounts();
		int i = 1;
		@SuppressWarnings("unchecked")
		List<String> mobileNumbersList = masterCollection.distinct("mobileNum");
		processedMobileNumbers = new HashSet<String>(mobileNumbersList);
		while (scanner.hasNextLine()) {
			String input = scanner.nextLine();
			if (input.equalsIgnoreCase("exit")) {
				scanner.close();
				System.exit(0);
			}
			if (input.equalsIgnoreCase("clean")) {
				shouldICleanUpAndStop = true;
			} else {
				sheetDetails.put("ThreadID:" + i, input);
				Thread thread = new Thread(classObj, "ThreadID:" + i);
				thread.start();
				i++;
			}
			System.out.println("Type exit to close the program or sheet name with location to process new sheet::");
		}
		scanner.close();
	}

	public void run() {
		String threadName = Thread.currentThread().getName();
		System.out.println(threadName + " Hulk has started processing :" + sheetDetails.get(threadName));
		validate(sheetDetails.get(threadName), threadName);
		System.out.println("\n\n\n\n" + threadName + " Hulk has finished processing :" + sheetDetails.get(threadName));
	}

	public static void validate(String sheetNameWithLocation, String threadName) {
		String fileName = sheetNameWithLocation.substring(sheetNameWithLocation.lastIndexOf("/") + 1);
		Workbook workbook = null;
		try (FileInputStream inputStream = new FileInputStream(sheetNameWithLocation);) {
			workbook = new XSSFWorkbook(inputStream);
			Sheet dataSheet = workbook.getSheet(AppConstants.DATA_SHEET);
			if (dataSheet != null) {
				String countryName = fileName.split("_")[0];
				if (AppConstants.COUNTRY_IDS.get(countryName) == null) {
					LOGGER.error("Country Not Mapped.");
					return;
				}
				long startTime = System.currentTimeMillis();
				for (int i = 1, count = 1; i <= dataSheet.getLastRowNum() && !shouldICleanUpAndStop; i++, count++) {
					try {
						validateAndProcessData(countryName, dataSheet, i, threadName);
					} catch (Exception e) {
						dataSheet.getRow(i).createCell(10).setCellValue(e.getMessage());
						e.printStackTrace();
						LOGGER.error(threadName + "::Exception occurred for record number:" + i + " Message is :: " + e.getMessage());
					}
					if (count != 0 && count % 100 == 0) {
						LOGGER.info(threadName + "::Hulk status is :: Processed " + count + " records in " + (System.currentTimeMillis() - startTime) + " ms");
						LOGGER.info(threadName + "::Containing " + productsMap.size() + " products details in map currently");
						LOGGER.info(threadName + "::Containing " + locationsMap.size() + " location details in map currently");
					}
				}
				LOGGER.info(threadName + "::Total time taken to process sheet is::" + (System.currentTimeMillis() - startTime) + " ms");
			} else {
				LOGGER.error(threadName + "::DATA sheet is not present. Program exiting....");
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Unable to locate file");
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("Unable to load or write to file.");
			e.printStackTrace();
		} finally {
			if (workbook != null) {
				try (FileOutputStream outputStream = new FileOutputStream(sheetNameWithLocation);) {
					workbook.write(outputStream);
				} catch (FileNotFoundException e) {
					LOGGER.error("Unable to locate file");
					e.printStackTrace();
				} catch (IOException e) {
					LOGGER.error("Unable to load or write to file.");
					e.printStackTrace();
				}
			}
		}
	}

	private static void validateAndProcessData(String countryName, Sheet dataSheet, int rowNumber, String threadName) {
		RegistrationTo objRegistrationTo = new RegistrationTo();
		String errorMessage = "";
		boolean inValidFound = false;
		Row row = dataSheet.getRow(rowNumber);
		//Name
		if (row.getCell(0) != null) {
			if (!row.getCell(0).getStringCellValue().trim().equals(""))
				objRegistrationTo.setName(row.getCell(0).getStringCellValue().trim());
			else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
		}
		//BizName
		if (row.getCell(1) != null) {
			if (!row.getCell(1).getStringCellValue().trim().equals(""))
				objRegistrationTo.setBusinessName(row.getCell(1).getStringCellValue().trim());
			else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
		}
		//BizTypeName
		if (row.getCell(3) != null) {
			ArrayList<String> bizTypeInfo = getBizTypeId(row.getCell(3).getStringCellValue().trim());
			if (bizTypeInfo != null) {
				objRegistrationTo.setBusinessTypeId(bizTypeInfo.get(0));
				objRegistrationTo.setBusinessTypeName(bizTypeInfo.get(1));
			} else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
		}
		//Location
		if (row.getCell(4) != null) {
			String locationName = pattern.matcher(row.getCell(4).getStringCellValue().trim()).replaceAll("");
			//PlaceTo objPlaceTo = locationSearch(AppConstants.COUNTRY_IDS.get(countryName), row.getCell(4).getStringCellValue());
			PlaceTo objPlaceTo = getlocation(AppConstants.COUNTRY_IDS.get(countryName), locationName);
			if (objPlaceTo != null) {
				synchronized (locationsMap) {
					locationsMap.put(locationName, objPlaceTo);
				}
				objRegistrationTo.setLocationTo(objPlaceTo);
				objRegistrationTo.setApmcTo(objPlaceTo);
			} else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_LOCATION);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_LOCATION);
		}
		//Products
		if (row.getCell(5) != null) {
			ArrayList<ProductTo> productToList = new ArrayList<ProductTo>();
			Set<ProductTo> productsSet = productsSearch(row.getCell(5).getStringCellValue(), AppConstants.COUNTRY_IDS.get(countryName));
			if (productsSet.size() > 0) {
				productToList.addAll(productsSet);
				objRegistrationTo.setLstOfProducts(productToList);
			} else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_PRODUCT);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_PRODUCT);
		}
		//Email
		if (row.getCell(6) != null) {
			objRegistrationTo.setEmail(row.getCell(6).getStringCellValue());
		}
		//Website
		if (row.getCell(7) != null)
			objRegistrationTo.setWebsite(row.getCell(7).getStringCellValue());
		//Address
		if (row.getCell(8) != null)
			objRegistrationTo.setAddress(row.getCell(8).getStringCellValue());
		//Other contact numbers
		if (row.getCell(9) != null)
			objRegistrationTo.setOtherPhoneNumbers(row.getCell(9).getStringCellValue());
		//Mobile with telecome code
		if (row.getCell(2) != null) {
			if (!row.getCell(2).getStringCellValue().trim().equals("")) {
				objRegistrationTo.setMobileNumber(row.getCell(2).getStringCellValue().trim());
			} else {
				inValidFound = true;
				errorMessage = errorMessage.concat("Problem with mobile no");
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat("Problem with mobile no");
		}
		if (!inValidFound) {
			String name = objRegistrationTo.getName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ")
					.replace("{", " ").replace("}", " ");
			String bizName = objRegistrationTo.getBusinessName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ")
					.replace("{", " ").replace("}", " ");
			List<BasicDBObject> list = new ArrayList<BasicDBObject>();
			list.add(new BasicDBObject("name", Pattern.compile("^" + name + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizName", Pattern.compile("^" + bizName + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizTypeId", objRegistrationTo.getBusinessTypeId()));
			list.add(new BasicDBObject("locationId", objRegistrationTo.getLocationTo().getPlaceId()));
			DBCursor masterCollectionDbCursor = masterCollection.find(new BasicDBObject("$and", list));
			if (masterCollectionDbCursor.hasNext()) {
				errorMessage = errorMessage.concat(AppConstants.INVALID_DATA);
				row.createCell(10).setCellValue(errorMessage);
			} else {
				objRegistrationTo.setMobileTelecomCode(AppConstants.COUNTRY_CODES.get(countryName));
				objRegistrationTo.setIsPreCreated(true);
				objRegistrationTo.setStatus(AppConstants.IN_QUEUE);
				objRegistrationTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceName());
				//objRegistrationTo.set_id(new ObjectId().toString());
				DBObject dbObject = (DBObject) JSON.parse(gson.toJson(objRegistrationTo));
				WriteResult result = dumpingCollection.insert(dbObject);
				System.out.println(result.getError());
				MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
				//objMasterCollectionTo.set_id(new ObjectId().toString());
				objMasterCollectionTo.setBizId(null);
				objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
				objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
				objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
				objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
				objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
				objMasterCollectionTo.setName(objRegistrationTo.getName());
				objMasterCollectionTo.setProfileId(null);
				objMasterCollectionTo.setStatus(AppConstants.IN_QUEUE);
				objMasterCollectionTo.setPrfCreatedDate(null);
				masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
				synchronized (processedMobileNumbers) {
					processedMobileNumbers.add(objRegistrationTo.getMobileNumber());
				}
			}
		} else {
			row.createCell(10).setCellValue(errorMessage);
		}
		LOGGER.error(threadName + "::" + rowNumber + " row is processed.");
		if (!errorMessage.isEmpty())
			LOGGER.error(threadName + "::Error message is :: " + errorMessage + ":: for row::" + rowNumber);
	}

	private static ArrayList<String> getBizTypeId(String bizTypeName) {
		ArrayList<String> bizTypeInfo = new ArrayList<String>();
		bizTypeName = bizTypeName.toLowerCase();
		switch (bizTypeName) {
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRITRADER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRITRADER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_AGRITRADER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIBROKER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIBROKER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_BROKER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_FARMER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_FARMER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_FARMER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_TRANSPORTER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_TRANSPORTER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_TRANSPORTER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIINPUT:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIINPUT);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_INPUTS);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRISOCIETY:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRISOCIETY);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_SOCIETY);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_WAREHOUSE:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_WAREHOUSE);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_WAREHOUSE);
			return bizTypeInfo;
		default:
			return null;
		}
	}

	private static PlaceTo getlocation(String countryId, String subStrings) {
		String comparator = "";
		String spellcheckWord = subStrings;
		PlaceTo objPlaceTo = null;
		try {
			comparator = subStrings.replaceAll(" ", " or ");
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
			spellcheckWord = java.net.URLEncoder.encode(spellcheckWord, "UTF-8");
			subStrings = subStrings.replaceAll("\\+", "%20AND%20");

			Response clientResponse = null;
			String URL = PlatformConstants.COMMONDATA_SEARCH + "(filter:location%20AND%20countryId:" + countryId + ")%20AND%20" + subStrings
					+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs&start=0&rows=5000";
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			if (responsedata.get("numFound").toString().equals("0")) {
				JSONObject temp = objJson.getJSONObject("spellcheck");
				JSONObject misSpellArray = null;
				JSONArray queryAndSuggestion = null;
				JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
				for (int i = 0; i < suggestionsArray.length(); i++) {
					if (suggestionsArray.get(i).toString().equalsIgnoreCase(comparator)) {
						misSpellArray = new JSONObject(suggestionsArray.get(i + 1).toString());
						queryAndSuggestion = misSpellArray.getJSONArray("suggestion");
					}
				}
				if (queryAndSuggestion != null) {
					subStrings = queryAndSuggestion.get(0).toString();
				} else {
					return objPlaceTo;
				}
				subStrings = subStrings.replaceAll(" ", "%20AND%20");
				URL = PlatformConstants.COMMONDATA_SEARCH + "(filter:location%20AND%20countryId:" + countryId + ")%20AND%20" + subStrings
						+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName&start=0&rows=5000";
				webTarget = client.target(URL);
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.get();
				output = clientResponse.readEntity(String.class);
				objJson = new JSONObject(output);
				try {
					if (objJson.has("response")) {
						responsedata = objJson.getJSONObject("response");
					} else {
						return objPlaceTo;
					}
				} catch (Exception e) {
					LOGGER.error("Unable to find response");
				}

			}
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			for (int i = 0; i < array.length();) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				return gson.fromJson(obj1.get("_json_").toString(), PlaceTo.class);
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objPlaceTo;
	}

	public static Set<ProductTo> productsSearch(String subStrings, String countryId) {
		Set<ProductTo> productsset = new HashSet<ProductTo>();
		String productIds = "";
		try {
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = subStrings.replaceAll(",", " ");
			//String stateId = locationId.substring(0, 7);
			countryId = countryId.replace(",", " ");
			String URL = "";
			try {
				subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
				subStrings = subStrings.replaceAll("\\+", "%20");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			int rows = 5000;
			Response clientResponse = null;
			URL = PlatformConstants.COMMONDATA_SEARCH + "(countryId:(" + countryId + ")%20AND%20(" + subStrings + "*))&wt=json&spellcheck.dictionary=test&spellcheck=true&"
					+ "qf=%20alsoKnownAs%5E4" + "%20productName%5E5" + "%20localeNames.displayName%5E3" + "%20company1%20crops%5E2%20ingredients1&start=0&rows=" + rows
					+ "&fl=_json_";
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				SearchProductTo objSearchProductTo = gson.fromJson(obj1.get("_json_").toString(), SearchProductTo.class);
				if (objSearchProductTo.getVarietyId().subSequence(9, 12).equals("000")) {
					if (productsMap.containsKey(objSearchProductTo.getProductId())) {
						productsset.add(productsMap.get(objSearchProductTo.getProductId()));
					} else {
						productIds = productIds.concat(objSearchProductTo.getProductId() + ";");
					}
				}
			}
			if (!productIds.isEmpty()) {
				List<ProductTo> productToList = getProductDetails(productIds.substring(0, productIds.length() - 1), countryId);
				if (productToList != null)
					productsset.addAll(productToList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productsset;
	}

	public static List<ProductTo> getProductDetails(String productIds, String countryId) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProductTo> productToLst = new ArrayList<ProductTo>();
		List<String> productDupCheck = new ArrayList<String>();
		String[] listofIds = productIds.split(";");
		try {
			String query = createproductInQuery(null, productIds);
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, countryId);
			for (int i = 1; i <= listofIds.length; i++) {
				preparedStatement.setString(i + 1, listofIds[i - 1]);
			}
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				if (!productDupCheck.contains(resultSet.getString("vsspsc_code"))) {
					ProductTo objProductTo = new ProductTo();
					objProductTo.setProductId(resultSet.getString("vsspsc_code"));
					objProductTo.setProductName(resultSet.getString("display_name"));
					objProductTo.setBaseUnitId(resultSet.getInt("unit_id") + "");
					objProductTo.setBaseUnitName(resultSet.getString("unit_name"));
					objProductTo.setCommodityId(resultSet.getString("commodity_id"));
					objProductTo.setCommodityName(resultSet.getString("commodity"));
					objProductTo.setVarietyId(resultSet.getString("variety_id"));
					objProductTo.setVarietyName(resultSet.getString("variety"));
					objProductTo.setScientific_name(resultSet.getString("scientific_name"));
					objProductTo.setUniversal_name(resultSet.getString("universal_name"));
					objProductTo.setNotes(resultSet.getString("notes"));
					objProductTo.setWeblinks(resultSet.getString("weblinks"));
					objProductTo.setAlsoKnownAs(resultSet.getString("also_known_as"));
					objProductTo.setImageUrl(resultSet.getString("image"));
					objProductTo.setQualifiers(resultSet.getString("qualifiers"));
					objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
					objProductTo.setGroupId(resultSet.getString("group_id"));
					objProductTo.setGroupName(resultSet.getString("group_name"));
					objProductTo.setCrops(resultSet.getString("crops"));
					objProductTo.setCompany(resultSet.getString("company_id"));
					objProductTo.setIntegredients(resultSet.getString("ingredients"));
					UnitDeatilsTo details = new UnitDeatilsTo();
					details.setUnitId(resultSet.getInt("unit_id"));
					details.setUnitName(resultSet.getString("unit_name"));
					details.setConversion(resultSet.getDouble("conversion"));
					objProductTo.getListofUnits().add(details);
					objProductTo.setCustomunits(getProductCustomUnits(objProductTo.getProductId(), countryId));
					productToLst.add(objProductTo);
					productDupCheck.add(objProductTo.getProductId());
					synchronized (productsMap) {
						productsMap.put(objProductTo.getProductId(), objProductTo);
					}
				}
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return productToLst;
	}

	private static String createproductInQuery(String sessionId, String productIds) {
		String query = " SELECT P.ingredients, P.crops, P.company_id,P.vsspsc_code, P.image,P.qualifiers, PL.display_name,P.commodity_id,P.commodity, P.variety_id,P.variety,PG.group_id,P.ALSO_KNOWN_AS,P.SCIENTIFIC_NAME,P.WEBLINKS,P.NOTES,"
				+ " P.UNIVERSAL_NAME,P.HSCODE, PG.group_name, U.unit_id, U.unit_name,U.conversion FROM Products P INNER JOIN Units U on P.unit_id = U.base_unit_id INNER JOIN PRODUCT_GROUPS PG on "
				+ " P.group_id = PG.group_id INNER JOIN PRODUCT_TO_COUNTRY PC on P.vsspsc_code = PC.vsspsc_code INNER JOIN PRODUCTS_LOCALE_NAMES PL on PL.vsspsc_code = PC.vsspsc_code WHERE PL.language_id =1 "
				+ " AND PC.country_id = PL.country_id AND PC.country_id = ? AND P.vsspsc_code IN (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < productIds.split(";").length; i++) {
			queryBuilder.append(" ?");
			if (i != productIds.split(";").length - 1) {
				queryBuilder.append(",");
			}
		}
		queryBuilder.append(") ORDER BY PL.display_name ASC");
		return queryBuilder.toString();
	}

	private static List<UnitDeatilsTo> getProductCustomUnits(String vsspscCode, String countryId) {
		PreparedStatement preparedStatement = null;
		List<UnitDeatilsTo> customUnitsList = new ArrayList<>();
		ResultSet resultSet = null;
		String query = "SELECT custom_unitid, custom_unitname, conversion FROM PRODUCTS_TO_CUSTOMUNITS PC" + " INNER JOIN CUSTOMUNITS C on PC.custom_unitid = C.custom_unitid"
				+ " WHERE PC.vsspsc_code = ? AND C.country_id = ?";
		try {
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, vsspscCode);
			preparedStatement.setString(2, countryId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				UnitDeatilsTo objUnitDeatilsTo = new UnitDeatilsTo();
				objUnitDeatilsTo.setUnitId(resultSet.getInt("custom_unitid"));
				objUnitDeatilsTo.setUnitName(resultSet.getString("custom_unitname"));
				objUnitDeatilsTo.setConversion(resultSet.getDouble("conversion"));
				customUnitsList.add(objUnitDeatilsTo);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			LOGGER.error("Problem while fetching Custom units...");
		}
		Collections.sort(customUnitsList, new Comparator<UnitDeatilsTo>() {
			@Override
			public int compare(UnitDeatilsTo o1, UnitDeatilsTo o2) {
				String placeName1 = o1.getUnitName();
				String placeName2 = o2.getUnitName();
				//ascending order
				return placeName1.compareTo(placeName2);
			}

		});
		return customUnitsList;
	}

	public static String qualifiersort(String qualifier) {
		StringBuilder strbui = new StringBuilder();
		if ((qualifier != null) && !(qualifier.equals(""))) {
			String[] array = qualifier.split(",");
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				strbui.append(array[i]);
				if ((i + 1) < array.length) {
					strbui.append(",");
				}
			}
			qualifier = strbui.toString();
		}
		return qualifier;
	}
}