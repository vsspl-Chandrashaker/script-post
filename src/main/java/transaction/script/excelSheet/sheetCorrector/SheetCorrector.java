package transaction.script.excelSheet.sheetCorrector;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import transaction.script.constants.AppConstants;

public class SheetCorrector {

	public static void main(String[] args) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/abhi/Desktop/log2.txt"));
			String sheetLocation = "/home/abhi/Desktop/sheets/problem2/Myanmar_AgriTrader_final1_3733.xlsx";
			Workbook workbook = new XSSFWorkbook(new FileInputStream(sheetLocation));
			Sheet sheet = workbook.getSheet("DATA");
			String currentLine, previousLine = null;
			String string1 = "ThreadID:4::";//make sure this corresponds to found on below=========================================================================================
			String string2 = " row is processed.";
			String errorString = "Error message is ::";
			int count = 0;
			while ((currentLine = bufferedReader.readLine()) != null) {
				if (currentLine.contains(string1)) {
					if (currentLine.contains("Exception occurred for record number:")) {
						TimeUnit.SECONDS.sleep(5);
						/*String errorMessage = AppConstants.INVALID_LOCATION + AppConstants.INVALID_LOCATION_PRODUCT;
						String foundOn = currentLine.substring(currentLine.indexOf("Exception occurred for record number:") + 37, currentLine.indexOf(" Message is ::"));
						System.out.println("??????????::" + foundOn);
						System.out.println("**********" + currentLine + "\n");
						System.out.println("yellow");
						System.out.println(errorMessage);
						//if (i == 15)
						System.out.println("Sleeping");
						sheet.getRow(Integer.valueOf(foundOn)).createCell(10).setCellValue(errorMessage);*/
					}
					if (currentLine.contains(errorString)) {
						count++;
						String foundOn = previousLine.substring(previousLine.indexOf(string1) + 12, previousLine.indexOf(string2));//currently at 13 make sure rey baba=============
						String errorMessage = currentLine.substring(currentLine.indexOf(errorString) + 19);
						System.out.println("++++++++++++++" + previousLine + "\n");
						System.out.println("**********" + currentLine + "\n");
						System.out.println("Found ya on::" + foundOn);
						System.out.println(errorMessage);
						System.out.println(sheet.getRow(Integer.valueOf(foundOn)).getCell(1));
						sheet.getRow(Integer.valueOf(foundOn)).createCell(10).setCellValue(errorMessage);
					} else {
						previousLine = currentLine;
					}
				}
			}
			System.out.println(count);
			try (FileOutputStream outputStream = new FileOutputStream(sheetLocation);) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
