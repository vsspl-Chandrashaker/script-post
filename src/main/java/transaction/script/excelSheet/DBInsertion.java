package transaction.script.excelSheet;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;

import transaction.script.models.DomainTo;
import transaction.script.platform.utils.PostgresDBUtil;

public class DBInsertion {
	static Gson gson = new Gson();

	public static void main(String[] args) throws Exception {
		//Connection conntestUsers = PostgresDBUtil.getConnection("users");
		//String linkInsertQuery = "INSERT INTO dev_master(mobile_number, data)VALUES(?,CAST(? AS jsonb))";
		//PreparedStatement linkpst = conntestUsers.prepareStatement(linkInsertQuery);
		FileInputStream fs = new FileInputStream("/home/vasudhaika/Downloads/test.xlsx");
		Workbook workbookFrom = new XSSFWorkbook(fs);
		Sheet sheetdoamins = workbookFrom.getSheetAt(1);
		Sheet sheetlinks = workbookFrom.getSheetAt(2);
		ArrayList<DomainTo> objDomainToslist = new ArrayList<DomainTo>();
		for (int rowNum = 0; rowNum < sheetdoamins.getLastRowNum() + 1; rowNum++) {
			DomainTo objDomainTo = new DomainTo();
			Row row = sheetdoamins.getRow(rowNum);
			if (row.getCell(0) != null && !row.getCell(0).toString().isEmpty()) {
				objDomainTo.setDomainName(row.getCell(0).getStringCellValue());
			}
			if (row.getCell(1) != null && !row.getCell(1).toString().isEmpty()) {
				objDomainTo.setDomainHitCount(row.getCell(1).getStringCellValue());
			}
			if (row.getCell(2) != null && !row.getCell(2).toString().isEmpty()) {
				String isValid = row.getCell(2).getStringCellValue();
				objDomainTo.setValidDomain(Boolean.parseBoolean(isValid));
			} else {
				objDomainTo.setValidDomain(Boolean.FALSE);
			}
			if (row.getCell(3) != null && !row.getCell(3).toString().isEmpty()) {
				objDomainTo.setUserCount(row.getCell(3).getStringCellValue());
			} else {
				objDomainTo.setUserCount("");
			}
			if (row.getCell(4) != null && !row.getCell(4).toString().isEmpty()) {
				objDomainTo.setPostCount(row.getCell(4).getStringCellValue());
			} else {
				objDomainTo.setPostCount("");
			}
			if (row.getCell(5) != null && !row.getCell(5).toString().isEmpty()) {
				String isGlobal = row.getCell(2).getStringCellValue();
				objDomainTo.setGlobal(Boolean.parseBoolean(isGlobal));
			} else {
				objDomainTo.setGlobal(Boolean.FALSE);
			}
			if (row.getCell(6) != null && !row.getCell(6).toString().isEmpty()) {
				objDomainTo.setCountryNames(row.getCell(6).getStringCellValue());
			} else {
				objDomainTo.setCountryNames("");
			}
			System.out.println(gson.toJson(objDomainTo));
			objDomainToslist.add(objDomainTo);
		}
		
		//insertDomains(objDomainToslist);
	}

	public static void insertDomains(ArrayList<DomainTo> objDomainToslist) throws Exception {
		Connection conntestUsers = PostgresDBUtil.getConnection("testdb");
		String DomainInsertQuery = "INSERT INTO domains_list(domain_name, data)VALUES(?,CAST(? AS jsonb))";
		PreparedStatement domainpst = conntestUsers.prepareStatement(DomainInsertQuery);
		for (int i = 0; i < objDomainToslist.size(); i++) {
			DomainTo objDomainTo = objDomainToslist.get(i);
			String data = gson.toJson(objDomainTo);
			domainpst.setString(1, objDomainTo.getDomainName());
			domainpst.setString(2, data);
			domainpst.addBatch();
		}
		domainpst.executeBatch();
	}
}
