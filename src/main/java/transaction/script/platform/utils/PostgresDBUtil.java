/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;

public class PostgresDBUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBUtil.class);
	static String postgresHostUrl = "jdbc:postgresql://" + AppConstants.POSTGRES_IP + ":5432/";

	public static Connection getConnection(String dbname) {
		Connection dbConnection = null;
		try {
			Class.forName("org.postgresql.Driver");
			LOGGER.info(postgresHostUrl + dbname);
			if (!dbname.equalsIgnoreCase("users"))
				postgresHostUrl = "jdbc:postgresql://192.168.1.11:5432/";
			dbConnection = DriverManager.getConnection(postgresHostUrl + dbname, "openerp", "openerp");
		} catch (Exception e) {
			LOGGER.error("Error in the Business DataBase Connection:");
			e.printStackTrace();
		}
		return dbConnection;
	}
}