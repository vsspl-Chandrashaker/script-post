package transaction.script.platform.porting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import transaction.script.models.MasterCollectionTo;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;
import transaction.script.platform.utils.PostgresDBUtil;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class MasterCollectionToMasterTable {
	/*static Connection connection = null;

	static {
		connection = PostgresDBUtil.getConnection("users");
	}*/

	/*public static void main2(String[] args) {
		Set<String> mobile = new HashSet<String>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("Select mobile_number from master");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				mobile.add(resultSet.getString(1));
			}
			System.out.println(mobile.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static void main(String[] args) {
		try {
			DB mongoDb = MongoDBUtil.getConnection();
			DBCollection collection = mongoDb.getCollection("masterCollectionOCT20");
			Connection connection = PostgresDBUtil.getConnection("users");
			connection.setAutoCommit(true);
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO master5 (mobile_number,data) VALUES (?,CAST(? AS jsonb))");
			Gson gson = new Gson();
			int count = 0;
			int exceptionCount = 0;
			//int success = 0, fail = 0;
			HashMap<MasterCollectionTo, String> masterData = new HashMap<MasterCollectionTo, String>();
			while (true) {
				System.out.println("Fetching records from mongo::" + count);
				DBCursor cursor = collection.find().skip(count).limit(10000);
				if (cursor.size() == 0)
					break;
				else {
					while (cursor.hasNext()) {
						count++;
						String data = cursor.next().toString();
						MasterCollectionTo objMasterCollectionTo = gson.fromJson(data, MasterCollectionTo.class);
						preparedStatement.setString(1, objMasterCollectionTo.getMobileNum());
						preparedStatement.setString(2, gson.toJson(objMasterCollectionTo));
						try {
							preparedStatement.execute();
							System.out.println("Inserting new batch into postgres: " + count);
						} catch (SQLException e) {
							System.err.println("size of reg data problems is :: " + exceptionCount);
							e.printStackTrace();
							exceptionCount++;
							masterData.put(objMasterCollectionTo, e.getMessage());
						}
						//preparedStatement.addBatch();
						/*if (count % 10 == 0) {
							try {
								for (int temp : preparedStatement.executeBatch()) {
									if (temp >= 0)
										success++;
									else
										fail++;
								}
							} catch (SQLException e) {
								e.printStackTrace();
								System.err.println("size of reg data problems is :: " + exceptionCount);
								exceptionCount++;
								masterData.put(objMasterCollectionTo, e.getNextException().getMessage());
							}
						}*/
					}
					//System.out.println(success + "***" + fail);
				}
			}
			preparedStatement.executeBatch();
			System.out.println(count);
			System.out.println("Final size of master data problems is :: " + masterData.size());
			System.out.println("Final size of master data problems is :: " + exceptionCount);
			for (MasterCollectionTo objMasterCollectionTo : masterData.keySet()) {
				System.out.println(gson.toJson(objMasterCollectionTo));
				System.out.println(masterData.get(objMasterCollectionTo));
				System.out.println("\n");
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}